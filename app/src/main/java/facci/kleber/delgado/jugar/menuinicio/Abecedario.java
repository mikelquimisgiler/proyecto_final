package facci.kleber.delgado.jugar.menuinicio;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;

import facci.kleber.delgado.jugar.R;

public class Abecedario extends AppCompatActivity {


    Button BTNA, BTNB, BTNC, BTND, BTNE, BTNF, BTNG, BTNH, BTNI, BTNJ, BTNK, BTNL, BTNM, BTNN, BTNÑ, BTNO, BTNP, BTNQ, BTNR, BTNS, BTNT, BTNU, BTNV, BTNW, BTNX, BTNY;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_abecedario);
        BTNA= (Button) findViewById(R.id.BTNA);
        BTNB= (Button) findViewById(R.id.BTNB);
        BTNC= (Button) findViewById(R.id.BTNC);
        BTND= (Button) findViewById(R.id.BTND);
        BTNE= (Button) findViewById(R.id.BTNE);
        BTNF= (Button) findViewById(R.id.BTNF);
        BTNG= (Button) findViewById(R.id.BTNG);
        BTNH= (Button) findViewById(R.id.BTNH);
        BTNI= (Button) findViewById(R.id.BTNI);
        BTNJ= (Button) findViewById(R.id.BTNJ);
        BTNK= (Button) findViewById(R.id.BTNK);
        BTNL= (Button) findViewById(R.id.BTNL);
        BTNM= (Button) findViewById(R.id.BTNM);
        BTNN= (Button) findViewById(R.id.BTNN);
        BTNÑ= (Button) findViewById(R.id.BTNÑ);
        BTNO= (Button) findViewById(R.id.BTNO);
        BTNP= (Button) findViewById(R.id.BTNP);
        BTNQ= (Button) findViewById(R.id.BTNQ);
        BTNR= (Button) findViewById(R.id.BTNR);
        BTNS= (Button) findViewById(R.id.BTNS);
        BTNT= (Button) findViewById(R.id.BTNT);
        BTNU= (Button) findViewById(R.id.BTNU);
        BTNV= (Button) findViewById(R.id.BTNV);
        BTNW= (Button) findViewById(R.id.BTNW);
        BTNX= (Button) findViewById(R.id.BTNX);
        BTNY= (Button) findViewById(R.id.BTNY);

        //Funcion de boton para vocal A
        BTNA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosVocales%2FA.mp3?alt=media&token=dd584c3f-44fd-40be-9513-9b7456a3633e");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal A", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal B
        BTNB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FB.mp3?alt=media&token=f5ec9197-d3fd-400c-a517-da1413b3b743");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal B", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal C
        BTNC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FC.mp3?alt=media&token=b1f92520-be2d-451f-a8a8-884f60caf15f");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal C", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal D
        BTND.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FD.mp3?alt=media&token=50d5cf9e-73f6-433b-a6b2-3f4d63ad56aa");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal D", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal E
        BTNE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FE.mp3?alt=media&token=18be37f0-d69c-484e-bbb9-3c5da82c22da");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal E", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal F
        BTNF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FF.mp3?alt=media&token=ad8453b0-ef9c-4e71-a95b-121e80ff2683");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal F", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal G
        BTNG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FG.mp3?alt=media&token=feb93ac7-6318-4004-aefa-38795dcd7160");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal G", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal H
        BTNH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FH.mp3?alt=media&token=66664e08-7905-4854-a8dc-2c276083f9d7");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal H", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal I
        BTNI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FI.mp3?alt=media&token=dd14ad87-f048-4a80-8b43-a3f64e71ddb0");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal I", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal J
        BTNJ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FJ.mp3?alt=media&token=fc40e222-7c70-48b3-9320-e98be479bb7e");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal J", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal K
        BTNK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FK.mp3?alt=media&token=a6581354-a16d-44a1-9238-1fa308f1570b");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal K", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal L
        BTNL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FL.mp3?alt=media&token=4f441e38-8beb-410a-b54a-3f30d0a0237d");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal L", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal M
        BTNM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FM.mp3?alt=media&token=509bd57a-0863-425b-ba4e-02f7f167cde8");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal M", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal N
        BTNN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FN.mp3?alt=media&token=0df6835f-9df8-4dfa-9371-eeab800d5607");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal N", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal Ñ
        BTNÑ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2F%C3%91.mp3?alt=media&token=47a44b9c-40a5-4392-8153-f434877f8eff");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal Ñ", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal O
        BTNO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FO.mp3?alt=media&token=1a9da247-aebf-4f24-ab74-6fedcf0532f8");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal O", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal P
        BTNP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FP.mp3?alt=media&token=9f97ed6a-2392-496a-98d7-b5db6cc6e2d3");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal P", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal Q
        BTNQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FQ.mp3?alt=media&token=af452621-d292-4c9b-9f7d-ad3f8ca513bb");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal Q", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal R
        BTNR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FR.mp3?alt=media&token=918f7e79-d4ef-4d5f-ae34-dcfa8b3bab81");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal R", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal S
        BTNS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FS.mp3?alt=media&token=a85aa020-4ce2-43c6-90f4-34add35ed3ab");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal S", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal T
        BTNT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FT.mp3?alt=media&token=15834d3d-8e30-4011-8b26-64ea58bef442");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal T", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal U
        BTNU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FU.mp3?alt=media&token=0bc0011d-6cd4-4e29-b5c4-db5b22c2e634");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal U", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal V
        BTNV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FV.mp3?alt=media&token=c169ba3d-0723-4e66-ab4d-54e1e1efba78");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal V", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal W
        BTNW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FW.mp3?alt=media&token=738f1a72-a339-4c20-ba30-cdcd80870029");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal W", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal X
        BTNX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FX.mp3?alt=media&token=f6c44541-75f6-4cfc-8f53-d06b1c5df70c");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal X", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal Y
        BTNY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosAbecedario%2FY.mp3?alt=media&token=a07be24a-b10e-4247-8429-d0789a0b0c6b");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal Y", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });

    }
}
