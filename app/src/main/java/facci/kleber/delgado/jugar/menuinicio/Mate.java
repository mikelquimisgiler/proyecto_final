package facci.kleber.delgado.jugar.menuinicio;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;

import facci.kleber.delgado.jugar.R;

public class Mate extends AppCompatActivity {

    Button BTN0, BTN1, BTN2, BTN3, BTN4, BTN5, BTN6, BTN7, BTN8,BTN9,BTN10 ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mate);
        BTN0= (Button) findViewById(R.id.BTN0);
        BTN1= (Button) findViewById(R.id.BTN1);
        BTN2= (Button) findViewById(R.id.BTN2);
        BTN3= (Button) findViewById(R.id.BTN3);
        BTN4= (Button) findViewById(R.id.BTN4);
        BTN5= (Button) findViewById(R.id.BTN5);
        BTN6= (Button) findViewById(R.id.BTN6);
        BTN7= (Button) findViewById(R.id.BTN7);
        BTN8= (Button) findViewById(R.id.BTN8);
        BTN9= (Button) findViewById(R.id.BTN9);
        BTN10= (Button) findViewById(R.id.BTN10);


        //Funcion de boton para el numero 0
        BTN0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosNumeros%2F0.mp3?alt=media&token=b8039495-d6db-44c3-ac06-b75f272554ca");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });//Funcion de boton para el numero 1
        BTN1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosNumeros%2F1.mp3?alt=media&token=59a4c83d-321a-4ffd-9072-43cc684cbb9e");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Numero 1", Toast.LENGTH_SHORT);
                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para el numero 2
        BTN2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosNumeros%2F2.mp3?alt=media&token=080bad17-cf11-4af9-9dd6-249d92ae484f");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Numero 2", Toast.LENGTH_SHORT);
                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para el numero 3
        BTN3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosNumeros%2F3.mp3?alt=media&token=fa6e9fb2-c556-41e9-a501-a132a0fdbae1");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Numero 3", Toast.LENGTH_SHORT);
                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para el numero 4
        BTN4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosNumeros%2F4.mp3?alt=media&token=00223aca-9f72-433b-9097-5d9d6abb139d");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Numero 4", Toast.LENGTH_SHORT);
                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para el numero 5
        BTN5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosNumeros%2F5.mp3?alt=media&token=6efa9015-9b4b-4c09-938c-ac0850177425");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Numero 5", Toast.LENGTH_SHORT);
                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para el numero 6
        BTN6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosNumeros%2F6.mp3?alt=media&token=31188c60-336d-4984-b5a1-1a54a8814463");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Numero 6", Toast.LENGTH_SHORT);
                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para el numero 7
        BTN7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosNumeros%2F7.mp3?alt=media&token=25ee5d74-920b-4fd1-81b1-de0b0cdb410c");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Numero 7", Toast.LENGTH_SHORT);
                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para el numero 8
        BTN8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosNumeros%2F8.mp3?alt=media&token=038a0c00-7d45-47a5-8b3f-8e872878ade6");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Numero 8", Toast.LENGTH_SHORT);
                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para el numero 9
        BTN9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosNumeros%2F9.mp3?alt=media&token=84bdf4a2-be7a-48c3-8a2e-2d1a906620f0");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Numero 9", Toast.LENGTH_SHORT);
                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });

        //Funcion de boton para el numero 10
        BTN10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosNumeros%2F10.mp3?alt=media&token=cc439965-1826-4b22-ae7b-8fb06811a3c2");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Numero 10", Toast.LENGTH_SHORT);
                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        }
}