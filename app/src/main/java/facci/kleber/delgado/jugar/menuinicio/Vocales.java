package facci.kleber.delgado.jugar.menuinicio;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.Toast;

import java.io.IOException;

import facci.kleber.delgado.jugar.R;

public class Vocales extends AppCompatActivity {

    Button BTNA, BTNE, BTNI, BTNO, BTNU;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vocales);
        BTNA= (Button) findViewById(R.id.BTNA);
        BTNE= (Button) findViewById(R.id.BTNE);
        BTNI= (Button) findViewById(R.id.BTNI);
        BTNO= (Button) findViewById(R.id.BTNO);
        BTNU= (Button) findViewById(R.id.BTNU);

        //Funcion de boton para vocal A
        BTNA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosVocales%2FA.mp3?alt=media&token=dd584c3f-44fd-40be-9513-9b7456a3633e");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal A", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal E
        BTNE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosVocales%2FE.mp3?alt=media&token=725f4863-630c-4e46-b263-327aee6dd41a");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal E", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal I
        BTNI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosVocales%2FI.mp3?alt=media&token=e73f94e9-75c2-46da-8e49-19201606bd43");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal I", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal O
        BTNO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosVocales%2FO.mp3?alt=media&token=164a9801-bc73-4655-99ee-2709e79ce649");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal O", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
        //Funcion de boton para vocal U
        BTNU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    //Ruta de sonido
                    mediaPlayer.setDataSource("https://firebasestorage.googleapis.com/v0/b/jugar-c401a.appspot.com/o/SonidosVocales%2FU.mp3?alt=media&token=bc69c447-2036-44c8-9685-5a195dee0512");
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            Toast.makeText(getApplicationContext(), "Vocal U", Toast.LENGTH_SHORT);

                        }
                    });
                    mediaPlayer.prepare();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
    }
}
